<?php

namespace Drupal\meta_conversions_api\Services;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\meta_conversions_api\Logger\FacebookLogger;
use FacebookAds\Api;
use FacebookAds\Object\ServerSide\Content;
use FacebookAds\Object\ServerSide\CustomData;
use FacebookAds\Object\ServerSide\Event;
use FacebookAds\Object\ServerSide\EventRequest;
use FacebookAds\Object\ServerSide\UserData;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Services class for MetaClient.
 *
 * @package Drupal\meta_conversions_api
 */
class MetaClient {

  /**
   * Facebook API.
   *
   * @var \FacebookAds\Api
   */
  protected Api $api;

  /**
   * The Meta conversions API config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The event config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $eventConfig;

  /**
   * Drupal module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Facebook logger.
   *
   * @var \Drupal\meta_conversions_api\Logger\FacebookLogger
   */
  protected $facebookLogger;

  /**
   * Event names.
   *
   * @var array[]|null
   *   An array of event names, or NULL if when it hasn't been loaded yet.
   *
   * @see self::eventNames()
   */
  protected $eventNames;

  /**
   * Is it allowed to send events to Meta.
   *
   * This is used for static caching in case multiple events fire on a single
   * page load.
   *
   * @var bool
   *
   * @see self::isAllowed()
   */
  protected $isAllowed;

  /**
   * MetaClient constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   An instance of Config Factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   Drupal module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache backend.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   * @param \Psr\Log\LoggerInterface $logger
   *   An instance of Logger Channel Factory.
   * @param \Drupal\meta_conversions_api\Logger\FacebookLogger $facebook_logger
   *   Facebook logger.
   */
  public function __construct(ConfigFactory $config_factory,
                              ModuleHandlerInterface $module_handler,
                              CacheBackendInterface $cache,
                              RequestStack $request_stack,
                              LoggerInterface $logger,
                              FacebookLogger $facebook_logger) {
    $this->config = $config_factory->get('meta_conversions_api.settings');
    $this->eventConfig = $config_factory->get('meta_conversions_api.events');
    $this->moduleHandler = $module_handler;
    $this->cache = $cache;
    $this->requestStack = $request_stack;
    $this->logger = $logger;
    $this->facebookLogger = $facebook_logger;

    if ($this->config->get('enabled')) {
      $accessToken = $this->config->get('access_token');
      if ($accessToken) {
        $this->api = Api::init(
          NULL,
          NULL,
          $accessToken,
          FALSE
        );

        if ($this->config->get('enable_logging')) {
          $this->api->setLogger($this->facebookLogger);
        }
      }
    }
  }

  /**
   * Check whether sending events is allowed.
   *
   * @return bool
   *   TRUE if no module has objected to sending events.
   *
   * @see hook_meta_conversions_api_allowed();
   */
  public function isAllowed() {
    if (!isset($this->isAllowed)) {
      $allowed = $this->moduleHandler->invokeAll('meta_conversions_api_allowed');

      if (empty($allowed)) {
        $this->isAllowed = TRUE;
        return TRUE;
      }

      /** @var \Drupal\Core\Access\AccessResultInterface $result */
      $result = array_shift($allowed);
      foreach ($allowed as $other) {
        $result = $result->orIf($other);
      }

      $this->isAllowed = !$result->isForbidden();
    }

    return $this->isAllowed;
  }

  /**
   * Check if Meta API is enabled.
   *
   * @return bool
   *   TRUE if enabled and able to send requests to Meta.
   */
  public function isEnabled() {
    // Can't send requests unless API is configured.
    if (!isset($this->api)) {
      return FALSE;
    }

    // Master off switch.
    if (!$this->config->get('enabled')) {
      return FALSE;
    }

    // Need a pixel ID.
    $pixel_id = $this->config->get('pixel_id');
    if (empty($pixel_id)) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Sends request to the Facebook client.
   *
   * @param array $eventData
   *   The event data. Must contain 'event_id' and 'event_name'. Other fields
   *   will be set to defaults if not specified.
   * @param array $userData
   *   Array of user data. If not provided client_ip_address and
   *   client_user_agent will be set automatically.
   * @param array $customData
   *   Array of custom data.
   * @param string|null $testEventCode
   *   Meta test event code.
   *
   * @see \FacebookAds\Object\ServerSide\Event::__construct()
   * @see \FacebookAds\Object\ServerSide\UserData::__construct()
   * @see \FacebookAds\Object\ServerSide\CustomData::__construct()
   */
  public function sendRequest(array $eventData, array $userData = [], array $customData = [], string $testEventCode = NULL): void {
    if (!$this->isEnabled()) {
      return;
    }

    if (!$this->isAllowed()) {
      return;
    }

    if (empty($eventData['event_id']) || empty($eventData['event_name'])) {
      $this->logger->error('Missing event_id or event_name in Meta request, called from ' . debug_backtrace(!DEBUG_BACKTRACE_PROVIDE_OBJECT | DEBUG_BACKTRACE_IGNORE_ARGS, 2)[1]['function']);
      return;
    }

    // Check that this event hasn't been disabled in the config.
    if (!$this->isEventEnabled($eventData['event_name'])) {
      return;
    }

    // Allow the event to be renamed.
    $eventNames = $this->eventNames();
    if (!empty($eventNames[$eventData['event_name']])) {
      $eventData['event_name'] = $eventNames[$eventData['event_name']];
    }

    // If the test event code isn't passed directly into the function
    // then try using configuration.
    if (empty($testEventCode)) {
      $testEventCode = $this->config->get('test_event_code');
    }

    $userDataObject = new UserData($userData);
    if (!isset($data['client_ip_address'])) {
      $userDataObject->setClientIpAddress($_SERVER['REMOTE_ADDR']);
    }
    if (!isset($data['client_user_agent'])) {
      $userDataObject->setClientUserAgent($_SERVER['HTTP_USER_AGENT']);
    }

    $customDataObject = new CustomData($customData);
    if (isset($customData['content'])) {
      $content = new Content($customData['content']);
      $customDataObject->setContents([$content]);
    }

    // Combine all the data into an event.
    $event = new Event($eventData);
    if (!isset($eventData['event_time'])) {
      $event->setEventTime(time());
    }
    if (!isset($eventData['action_source'])) {
      $event->setActionSource('website');
    }
    if (!isset($eventData['event_source_url'])) {
      $currentRequest = $this->requestStack->getCurrentRequest();
      $event->setEventSourceUrl($currentRequest->getSchemeAndHttpHost() . $currentRequest->getRequestUri());
    }
    $event->setUserData($userDataObject)
      ->setCustomData($customDataObject);

    $events[] = $event;

    $request = (new EventRequest($this->config->get('pixel_id')))
      ->setEvents($events)
      ->setTestEventCode($testEventCode);

    try {
      // Send request over to Meta.
      $request->execute();
    }
    catch (\Exception $exception) {
      $this->logger
        ->error('Meta failed to send the request: ' . $exception->getMessage());
    }

  }

  /**
   * Get event names.
   *
   * @return array
   *   Array of event names, where the keys are the declared names and the
   *   values are the names to be used.
   */
  public function eventNames() {
    if (is_null($this->eventNames)) {
      $cacheId = 'meta_conversions_api_event_names';
      $cache = $this->cache->get($cacheId);
      if ($cache) {
        $this->eventNames = $cache->data;
      }
      else {
        $eventNames = $this->moduleHandler->invokeAll('meta_conversions_api_event_names');
        $this->eventNames = array_combine($eventNames, $eventNames);
        $this->moduleHandler->alter('meta_conversions_api_event_names', $this->eventNames);
        $this->cache->set($cacheId, $this->eventNames, CacheBackendInterface::CACHE_PERMANENT);
      }
    }

    return $this->eventNames;
  }

  /**
   * Check whether an event should be sent.
   *
   * @param string $eventName
   *   The event name as declared.
   *
   * @return bool
   *   TRUE if the event is enabled, FALSE otherwise.
   */
  public function isEventEnabled($eventName) {
    $eventNames = $this->eventNames();
    if (isset($eventNames[$eventName])) {
      $eventToggles = $this->eventConfig->get('event_toggles');
      if (isset($eventToggles[$eventName]) && !$eventToggles[$eventName]) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
