<?php

namespace Drupal\meta_conversions_api;

use Drupal\meta_conversions_api\Services\MetaClient;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension to allow checking the Meta status from Twig templates.
 */
class MetaApiTwig extends AbstractExtension {

  /**
   * Meta API Client.
   *
   * @var \Drupal\meta_conversions_api\Services\MetaClient|null
   */
  protected $metaClient;

  /**
   * Get Meta API client.
   *
   * @return \Drupal\meta_conversions_api\Services\MetaClient
   *   The Meta API client.
   */
  protected function metaClient() {
    if (!isset($this->metaClient)) {
      $this->metaClient = \Drupal::service('meta_conversions_api.meta_client');
    }

    return $this->metaClient;
  }

  /**
   * Set the Meta client manually instead of using the container.
   *
   * @param \Drupal\meta_conversions_api\Services\MetaClient $metaClient
   *   The Meta API client.
   */
  protected function setMetaClient(MetaClient $metaClient) {
    $this->metaClient = $metaClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('meta_api_is_enabled', [$this, 'isEnabled']),
      new TwigFunction('meta_api_is_allowed', [$this, 'isAllowed']),
    ];
  }

  /**
   * Check if Meta API is enabled and configured.
   *
   * @return bool
   *   TRUE if Meta events can be sent.
   *
   * @see \Drupal\meta_conversions_api\Services\MetaClient::isEnabled()
   */
  public function isEnabled() {
    return $this->metaClient()->isEnabled();
  }

  /**
   * Check if Meta API is allowed to send requests.
   *
   * It can be blocked by a module, for example, if the user has not consented
   * to be tracked.
   *
   * @return bool
   *   TRUE if Meta events should be sent.
   *
   * @see \Drupal\meta_conversions_api\Services\MetaClient::isAllowed()
   */
  public function isAllowed() {
    return $this->metaClient()->isAllowed();
  }

}