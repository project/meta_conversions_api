<?php

namespace Drupal\meta_conversions_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use FacebookAds\Object\ServerSide\ActionSource;

/**
 * Settings form for Meta conversions API.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'meta_conversions_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'meta_conversions_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('meta_conversions_api.settings');

    $form['meta_conversions_api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Meta conversions API settings'),
    ];

    $form['meta_conversions_api']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Meta conversions API'),
      '#description' => $this->t('When disabled no events will be sent.'),
      '#default_value' => $config->get('enabled'),
    ];

    $form['meta_conversions_api']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access token'),
      '#description' => $this->t('You can generate this at @meta
        <ul>
          <li>Select the appropriate App under <em>Facebook App</em></li>
          <li>Then from <em>User or Page</em> select your listed page from <em>Page Access Tokens</em></li>
        </ul>
        ', ['@meta' => 'https://developers.facebook.com/tools/explorer/']),
      '#default_value' => $config->get('access_token'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $form['meta_conversions_api']['pixel_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pixel ID'),
      '#description' => $this->t('This is available at @meta', ['@meta' => 'https://business.facebook.com/overview/']),
      '#default_value' => $config->get('pixel_id'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];

    $actionSources = ActionSource::getInstance()->getValues();
    $form['meta_conversions_api']['default_action_source'] = [
      '#type' => 'select',
      '#title' => $this->t('Default event action source'),
      '#description' => $this->t('The default action source when the event does not specify one.'),
      '#options' => array_combine($actionSources, $actionSources),
      '#default_value' => $config->get('default_action_source'),
      '#required' => TRUE,
    ];

    $form['debug'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Debugging'),
    ];

    $form['debug']['test_event_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Test event code'),
      '#description' => $this->t('This is available at @meta in the form "TEST1234"', ['@meta' => 'https://business.facebook.com/events_manager2']),
      '#default_value' => $config->get('test_event_code'),
      '#size' => 60,
      '#maxlength' => 255,
    ];

    $form['debug']['enable_logging'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable logging'),
      '#description' => $this->t('Enable logging for the Facebook SDK and debugging of requests and responses.'),
      '#default_value' => $config->get('enable_logging'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('meta_conversions_api.settings')
      ->set('enabled', $form_state->getValue('enabled'))
      ->set('access_token', trim($form_state->getValue('access_token')))
      ->set('pixel_id', trim($form_state->getValue('pixel_id')))
      ->set('default_action_source', $form_state->getValue('default_action_source'))
      ->set('test_event_code', trim($form_state->getValue('test_event_code')))
      ->set('enable_logging', $form_state->getValue('enable_logging'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
