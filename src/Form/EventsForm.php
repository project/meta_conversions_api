<?php

namespace Drupal\meta_conversions_api\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\meta_conversions_api\Services\MetaClient;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Config form for Meta conversions API events.
 */
class EventsForm extends ConfigFormBase {

  /**
   * The Meta client.
   *
   * @var \Drupal\meta_conversions_api\Services\MetaClient
   */
  protected $metaClient;

  /**
   * Constructs an EventsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\meta_conversions_api\Services\MetaClient $meta_client
   *   The Meta client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MetaClient $meta_client) {
    parent::__construct($config_factory);

    $this->metaClient = $meta_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('meta_conversions_api.meta_client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'meta_conversions_api_events_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'meta_conversions_api.events',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('meta_conversions_api.events');

    $form['intro'] = [
      '#markup' => $this->t('Uncheck an event to prevent it from being sent to Meta. New events will be discovered when the cache is cleared.'),
    ];

    $eventNames = $this->metaClient->eventNames();
    // Default out of the box is any new event will be enabled. So get any
    // that have been specifically disabled as the ones to remove.
    $disabled = array_keys(array_filter($config->get('event_toggles') ?? [], function ($v) {
      return !$v;
    }));

    $form['event_toggles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Events'),
      '#options' => $eventNames,
      '#default_value' => array_diff(array_keys($eventNames), $disabled),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('meta_conversions_api.events')
      ->set('event_toggles', $form_state->getValue('event_toggles'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
