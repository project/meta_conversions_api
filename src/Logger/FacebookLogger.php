<?php

namespace Drupal\meta_conversions_api\Logger;

use FacebookAds\Http\RequestInterface;
use FacebookAds\Http\ResponseInterface;
use FacebookAds\Logger\LoggerInterface as FacebookLoggerInterface;
use Psr\Log\LoggerInterface;

/**
 * Implements Facebook LoggerInterface and logs to the Drupal log.
 *
 * The Facebook LoggerInterface is incompatible with Psr one in Drupal.
 */
class FacebookLogger implements FacebookLoggerInterface {

  /**
   * Drupal logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The Drupal logger.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = array()) {
    $this->logger->log($level, $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function logRequest($level, RequestInterface $request, array $context = array()) {
    $this->log($level, 'Meta API request: method @method. Query params: @query_params. Body params: @body_params', [
      '@method' => $request->getMethod(),
      '@query_params' => json_encode($request->getQueryParams()),
      '@body_params' => json_encode($request->getBodyParams()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function logResponse($level, ResponseInterface $response, array $context = array()) {
    $this->log($level, 'Meta API response: @status_code. Body: @body', [
      '@status_code' => $response->getStatusCode(),
      '@body' => json_encode($response->getBody()),
    ]);
  }

}
