<?php

namespace Drupal\meta_conversions_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\meta_conversions_api\Services\MetaClient;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for page view events.
 */
class PageViewController extends ControllerBase {

  /**
   * Meta client.
   *
   * @var \Drupal\meta_conversions_api\Services\MetaClient
   */
  protected $client;

  /**
   * Constructor for the controller.
   *
   * @param \Drupal\meta_conversions_api\Services\MetaClient $client
   *   Meta client.
   */
  public function __construct(MetaClient $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('meta_conversions_api.meta_client')
    );
  }

  /**
   * Respond to page view AJAX events.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   * @param string $event_name
   *   The event name.
   * @param string $event_id_callback
   *   Function to call to get the event ID. Will be passed the URL.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Empty response.
   */
  public function pageView(Request $request, $event_name, $event_id_callback) {
    $url = $request->query->get('url');
    if (!$url) {
      return new JsonResponse('No URL');
    }

    if (!is_callable($event_id_callback)) {
      throw new \InvalidArgumentException('No such function for $event_id_callback: ' . $event_id_callback);
    }

    $this->client->sendRequest([
      'event_name' => $event_name,
      'event_id' => call_user_func($event_id_callback, $url),
      'event_source_url' => $url,
    ]);

    // The response is not used.
    return new JsonResponse();
  }

  /**
   * Get the event ID for this page view event.
   *
   * @return string
   *   Event ID.
   */
  public function eventId() {
    return 'page-view-' . time() . '-' . mt_rand();
  }

}
