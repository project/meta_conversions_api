/**
 * @file
 * Meta conversions API page view AJAX triggering.
 */
(function ($, Drupal, drupalSettings, once) {

  Drupal.meta_conversions_api = {};

  /**
   * Check whether the API should send events to Meta.
   *
   * To send, the module needs to be enabled and configured and the allowed
   * callback needs to be permitted.
   */
  Drupal.meta_conversions_api.shouldSend = function (eventName) {
    // May have been added to the page as a dependency of another library, but should be ignored (e.g. on admin pages).
    if (!drupalSettings.hasOwnProperty('meta_conversions_api')) {
      return false;
    }

    // Disabled globally by config.
    if (!drupalSettings.meta_conversions_api.enabled) {
      return false;
    }

    // Disabled by setting a callback function.
    if (!Drupal.meta_conversions_api.allowedCallback()) {
      return false;
    }

    // Event is individually disabled in config.
    return drupalSettings.meta_conversions_api.events[eventName];
  };

  /**
   * Check whether it is allowed to send events to Meta.
   *
   * To override this behaviour, replace this function in a module that depends
   * on meta_conversions_api.
   *
   * @returns {boolean}
   */
  Drupal.meta_conversions_api.allowedCallback = function () {
    return true;
  }

  Drupal.behaviors.metaPageView = {
    attach: function (context) {

      if (!once('meta-cversions-api-page-view', 'html').length) {
        // Only execute the page view event once per page.
        return;
      }


      if (Drupal.meta_conversions_api.shouldSend('PageView')) {
        $.ajax({
          url: '/ajax/page-view',
          data: {
            'url': window.location.href,
          }
        });
      }
    }
  };

})(jQuery, Drupal, drupalSettings, once);
