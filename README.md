# Meta Conversions API

Allows integrating with Meta Conversions API.

## Requirements

This module requires no modules outside of Drupal core.

## Installation

1. Use composer to require the module and make sure facebook/php-business-sdk
   has also been installed.

## Configuration

1. Go to the settings page, enter the Access Token and Pixel ID from
   Facebook.
2. Make sure the checkbox to enable Meta Conversions API is checked.

## Usage

### Consent

To require consent, you must implement `hook_meta_conversions_api_allowed()`
(for example, checking a cookie).

It is also recommended to override the
`Drupal.meta_conversions_api.allowedCallback` function in your own JavaScript
library, as this will avoid unnecessarily making requests to the server.

## Adding events

You can implement `hook_meta_conversions_api_event_names()` to add your own
events. This is only necessary if you intend on making it possible to disable
the event.

You can rename existing events with
`hook_meta_conversions_api_event_names_alter().`

You can send your own events with the `sendRequest()` method of the
`MetaClient` service:

```
\Drupal::service('meta_conversions_api.meta_client')->sendRequest([
  'event_name' => 'MyEvent',
  'event_id' => 'my-event-' . time() . '-' . mt_rand(),
], [
  'first_name' => 'Test',
  'last_name' => 'Test',
], [
  'status' => 'registered',
]);
```

You do not need to hash the data, the Facebook SDK does this for you.

## Twig

In twig templates you can use the `meta_api_is_enabled()` method to check
whether Meta Conversions API is enabled and configured, and you can use the
`meta_api_is_allowed()` method to check whether events should be sent to Meta
(i.e. consent has been given).

## Maintainers

- Robert Mason - [Rob230](https://www.drupal.org/u/rob230)
