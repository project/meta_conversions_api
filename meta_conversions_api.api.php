<?php

/**
 * Add event(s) to be tracked by Meta conversions API.
 *
 * This hook should be implemented for each event, although it is not essential.
 * Implementing this hook will allow the event name to be altered by other
 * modules or the event to be disabled on the events config page.
 *
 * @return string[]
 *   Array of event names.
 */
function hook_meta_conversions_api_event_names() {
  return [
    'MyEvent',
  ];
}

/**
 * Modify event names used by the Meta conversions API.
 *
 * @param array $event_names
 *   Associative array. The keys are the defined event names, the values are
 *   what the event should be changed to.
 */
function hook_meta_conversions_api_event_names_alter(array &$event_names) {
  $event_names['MyEvent'] = 'DifferentName';
}

/**
 * Allows stopping the sending of any Meta conversions events.
 *
 * If you implement this hook based on a cookie or session then you may also
 * want to add a cache context with hook_page_attachments() for non-admin
 * routes, and override Drupal.meta_conversions_api.allowedCallback().
 *
 * @return \Drupal\Core\Access\AccessResultInterface
 *   Events will be sent unless a hook has returned AccessResultDenied.
 */
function hook_meta_conversions_api_allowed() {
  return \Drupal\Core\Access\AccessResult::forbiddenIf(!\Drupal::service('my_service')->consent());
}
